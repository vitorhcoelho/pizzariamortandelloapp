package pizzaria.donmortandello;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

public class Cardapio extends AppCompatActivity {

    private AdapterMenu adapterMenu;
    private RecyclerView recyclerView;
    private static List<Pizza> listPizza;
    private static final String DATA_REQUEST = "data_request";
    private static final String ORIGIN = "origin";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cardapio);

        if(listPizza == null){
            listPizza = new ArrayList<>();
        }

        int origin = getIntent().getIntExtra(ORIGIN, 2);
        if(origin == 2){
            Pizza pizza = (Pizza)getIntent().getParcelableExtra(DATA_REQUEST);
            Log.d("MENU_PIZZA_REQUEST", "" + pizza);
            listPizza.add(pizza);
        }

        adapterMenu = new AdapterMenu(listPizza);

        //Config recyclerView
        recyclerView = findViewById(R.id.recyclerMenu);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayout.VERTICAL));
        recyclerView.setAdapter(adapterMenu);

    }
}
