package pizzaria.donmortandello;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private static final String ORIGIN = "origin";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    //===============================================================================================
    public void goToRegisterPizza(View view){
        Intent intent = new Intent(this, CadastrarPizzas.class);
        startActivity(intent);
    }

    //===============================================================================================
    public void goToMenu(View view){

        Intent intent = new Intent(this, Cardapio.class);
        intent.putExtra(ORIGIN, 1);
        startActivity(intent);
    }
}
