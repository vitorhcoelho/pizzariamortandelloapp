package pizzaria.donmortandello;

import android.os.Parcel;
import android.os.Parcelable;

public class Pizza implements Parcelable {

    private String name;
    private Double price;
    private String description;

    //==================================================================================================
    public Pizza() {
    }

    //==================================================================================================

    public Pizza(String name, Double price, String description) {
        this.name = name;
        this.price = price;
        this.description = description;
    }

    //==================================================================================================

    public Pizza(Parcel in) {
        name = in.readString();

        price = in.readDouble();

        description = in.readString();
    }

    //==================================================================================================
    public static final Creator<Pizza> CREATOR = new Creator<Pizza>() {
        @Override
        public Pizza createFromParcel(Parcel in) {
            return new Pizza(in);
        }

        //==================================================================================================
        @Override
        public Pizza[] newArray(int size) {
            return new Pizza[size];
        }
    };

    //==================================================================================================
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    //==================================================================================================
    @Override
    public int describeContents() {
        return hashCode();
    }

    //==================================================================================================
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeDouble(price);
        dest.writeString(description);
    }

    //==================================================================================================
    @Override
    public String toString() {
        return "Pizza{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                '}';
    }
}
