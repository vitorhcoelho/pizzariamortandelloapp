package pizzaria.donmortandello;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class AdapterMenu extends RecyclerView.Adapter<AdapterMenu.mViewHolder> {

    public static List<Pizza> listPizzas;
    public static Pizza pizza;

    //=======================================================================================================================================
    public AdapterMenu() {
    }

    //=======================================================================================================================================
    public AdapterMenu(List<Pizza> listPizzas) {
        this.listPizzas = listPizzas;
    }

    //=======================================================================================================================================
    @NonNull
    @Override
    public mViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sample_menu, parent, false);
        return new mViewHolder(view);
    }

    //=======================================================================================================================================
    @Override
    public void onBindViewHolder(@NonNull mViewHolder holder, int position) {
        pizza = listPizzas.get(position);

        holder.pizzaName.setText(pizza.getName() + " - ");
        holder.pizzaPrice.setText("R$ " + Double.toString(pizza.getPrice()));
        holder.pizzaDescription.setText(pizza.getDescription());
    }

    //=======================================================================================================================================
    @Override
    public int getItemCount() {
        return listPizzas.size();
    }

    //=======================================================================================================================================

    /**
     * InnerClass para definir o viewHolder da parentClass
     */
    public class mViewHolder extends RecyclerView.ViewHolder {

        TextView pizzaName;
        TextView pizzaPrice;
        TextView pizzaDescription;

        //=======================================================================================================================================
        public mViewHolder(@NonNull View itemView) {
            super(itemView);

            pizzaName = itemView.findViewById(R.id.pizzaName);
            pizzaPrice = itemView.findViewById(R.id.pizzaPrice);
            pizzaDescription = itemView.findViewById(R.id.pizzaDescription);
        }
    }

}
