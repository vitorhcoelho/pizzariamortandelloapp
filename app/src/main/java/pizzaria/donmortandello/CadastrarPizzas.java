package pizzaria.donmortandello;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;


public class CadastrarPizzas extends AppCompatActivity {

    private EditText pizzaName;
    private EditText pizzaPrice;
    private EditText pizzaDescription;
    private static final String DATA_REQUEST = "data_request";
    ViewGroup parent;

    //==============================================================================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar_pizzas);

        pizzaName = findViewById(R.id.editTextPizzaName);
        pizzaPrice = findViewById(R.id.editTextPrice);
        pizzaDescription = findViewById(R.id.editTextDescription);
        parent = findViewById(R.id.registerPizza);
    }

    //==============================================================================================
    public void savePizza(View view){
        if(!validForm(parent)){
            Pizza pizza = new Pizza(pizzaName.getText().toString(), Double.parseDouble(pizzaPrice.getText().toString()),
                    pizzaDescription.getText().toString());
            Log.d("CADASTRAR_PIZZA", "" + pizza);
            Intent intent = new Intent(this, Cardapio.class);
            intent.putExtra(DATA_REQUEST, pizza);
            startActivity(intent);
        }
    }

    //==============================================================================================
    private boolean validForm(ViewGroup parent) {
        boolean isInvalidContent = false;
        EditText editText;

        try {
            for (int i = 0; i < parent.getChildCount(); i++) {
                View currentChild = parent.getChildAt(i);
                if (currentChild instanceof TextInputLayout) {
                    View frameLayout = ((ViewGroup) currentChild).getChildAt(0);
                    if (frameLayout instanceof FrameLayout) {
                        editText = (EditText) ((FrameLayout) frameLayout).getChildAt(0);
                        if (editText.getText().toString().isEmpty()) {
                            isInvalidContent = true;
                            String error = editText.getTag().toString();
                            ((TextInputLayout) currentChild).setError(error);
                            Log.d("VALID_FORM", "Empty Field -> " + error);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.w("VALID_FORM", "-> " + e.getMessage());
        }

        Log.d("VALID_FORM", "-> isInvalidContent: " + isInvalidContent);
        return isInvalidContent;
    }
}
